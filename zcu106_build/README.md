Starting with the Yocto :

Resources: 1]https://www.yoctoproject.org/docs/2.7.1/mega-manual/mega-manual.html<br/>
           2]https://twiki.cern.ch/twiki/bin/view/SystemOnChip/GettingStartedWithYocto

Yocto  provides tools and environment to build your customized linux disrtibution with toolchain, source etc. .
Somewhat substitute for buildroot. It has poky as distribution - which provideds metadata for building the environment for the specific board.


1] download the standalone and environment for yocto and always need to call this command/script, for setting up environment variables:<br/>
   source /opt/poky/2.2.1/environment-setup-x86_64-pokysdk-linux


2] Install pyro, openembedded core, metadata, meta-xilinx by cloning from repository.<br/>
   git clone git://git.yoctoproject.org/poky<br/>
   cd poky && git checkout  warrior && cd -

   git clone git://git.openembedded.org/openembedded-core<br/>
   cd openembedded-core && git checkout warrior && cd -

   git clone git://git.openembedded.org/meta-openembedded<br/>
   cd meta-openembedded && git checkout warrior  && cd -

   git clone git://git.yoctoproject.org/meta-xilinx<br/>
   cd meta-xilinx && git checkout -b warrior && cd -
   
   git clone https://github.com/Xilinx/meta-xilinx-tools.git<br/>
   cd meta-xilinx-tools &&  git checkout master
   


3] Process to build yocto images:<br/>
   cd poky<br/>
   source oe-init-build-env
   
The above commands will create a build directory with build directory with conf file and consists of default 
 local.conf and bblayers.conf files

4] If you landup with some dependency issues install additional packages
$sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib \
     build-essential chrpath socat cpio python python3 python3-pip python3-pexpect \
     xz-utils debianutils iputils-ping


   IMPORTANT : For building kernel image for zcu106 choose branch warrior for all layers

5] $bitbake core-image-minimal


Build Configuration:
BB_VERSION           = "1.42.0"<br/>
BUILD_SYS            = "x86_64-linux"<br/>
NATIVELSBSTRING      = "universal"<br/>
TARGET_SYS           = "aarch64-poky-linux"<br/>
MACHINE              = "zcu106-zynqmp"<br/>
DISTRO               = "poky"<br/>
DISTRO_VERSION       = "2.7.1"<br/>
TUNE_FEATURES        = "aarch64"<br/>
TARGET_FPU           = ""<br/>
meta                 <br/>
meta-poky            <br/>
meta-yocto-bsp       = "warrior:79a850a10a4b88a6d20d607b322542f947874323"<br/>
meta-oe              <br/>
meta-python          <br/>
meta-filesystems     = "warrior:f4ccdf2bc3fe4f00778629088baab840c868e36b"<br/>
meta-xilinx-bsp      <br/>
meta-xilinx-standalone <br/>
meta-xilinx-contrib  = "warrior:391c7054e88ae77abf18fe8a705ac7ff34c7dc79"<br/>
meta-xilinx-tools    = "master:881132b62dc01e57b8d9ace458a0005528179e14"<br/>

5] Some additonal fix, if bitbake fails:<br/>

Add warrior in meta-xilinx-tools/conf/local.conf<br/>

6] The yocto built images will be available in /tmp/deploy/images  under build directory:<br/>
command for resolving symlinks and copying to sd card:<br/>
rsync --archive --copy-links --recursive ~/poky/build/tmp/deploy/images/zcu106-zynqmp/  <path to destination folder><br/>

7] You can also check configurations with bblayers.conf and local.conf files attached in the respository.